﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerationScript : MonoBehaviour
{
    public GameObject ObstaclePrefab;
    public float WorldRadius = 9;

    // Use this for initialization
    void Start()
    {
        Vector3 center = transform.position;

        for (var i = 0; i < 10; i++)
        {
            Vector3 pos = RandomPositionOnCircle(center);

            Vector3 normal = (pos - center).normalized;
            Quaternion rotation = Quaternion.LookRotation(normal);
            GameObject go = Instantiate(ObstaclePrefab, pos, rotation);
            go.transform.SetParent(transform);
            go.transform.localScale = new Vector3(0.05f, 0.01f, 0.05f);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    Vector3 RandomPositionOnCircle(Vector3 center)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + WorldRadius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + WorldRadius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
}
