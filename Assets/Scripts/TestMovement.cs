﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    private bool jump = false;
    private Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Jump");
            jump = true;
        }
	}

    void FixedUpdate()
    {
        if (jump)
        {
            rb.AddForce(transform.up * 200);
            jump = false;
        }
    }
}
